# Wrath map editor support

This is a gamepack for [NetRadiant](https://netradiant.gitlab.io) providing support for Wrath: Aeon of Ruin.

This gamepack uses FGD definitions by xaGe for TrenchBroom (under GPLv3 license).
